<?php

class Tree {
    public $index = 1;
    public $id;
    public $title;
    public $arr = [];

    public function add($index, $id, $title){
    	$this->arr[] = "$index, $id, $title";
    }
    public function view(){
    	for($i=0;$i<=count($this->arr);$i++){
    		echo $this->arr[$i];
    		?>
			<br>
    		<?php
    	}
    }
}

$tree = new Tree();

$tree->add(1,null,'первый');
$tree->add(2,1,'второй');
$tree->add(3,1,'третий');
$tree->add(4,2,'четвертый');
$tree->add(5,2,'пятый');
$tree->add(6,7,'шестой');
$tree->add(7,1,'седьмой');
$tree->add(8,3,'восьмой');
$tree->add(9,9,'девятый');
$tree->view();